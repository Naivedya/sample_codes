//MNIT_IIITK_FAST_TRACK_ANDROID_DAY_5    

## Task Stack

1. Explain the concept of task stack using diffrent apps.

1. Show the empty task stack.

1. Message,Contacts,Call app using back 

1. Show the task stack with 3 apps. (Call,people,message)

1. Explain the youtube app with sharing on email.

## Back Stack6

1. Intro the Back Stack

1. Explain the concept of back Stack using Activtity Example.

1. Click Home and Open a browser

1. Activities within the same app ( Implicit ) with back button usage

1. Activities within different app ( Explicit ) with back button usage

1. Click Home and Open a browser

1. Lot of Apps in the back stack

1. Show how to add breakpoint and debug the code


1. Introduce the concept of Step Over by explaining excution of onCreate.

1. Resume Debugger on } of onCreate

1. Show the notificaiton bar and explain the visible sequence.

1. Add break point for onPasuse,onStop and onDestroy.

1. Click the back key and show onPasuse,onStop and onDestroy with breakpoints.

1. Show the notificaiton bar and explain the Stop sequence.

1. Open the Screen 2 and expain the pause lifecycle.

1. Show the notificaiton bar and explain the pause sequence.

1. Explain How the activity 2 is created keeping activity 1 in pause then creating the activity 2 and then pausing the activity 1.

1. If onStop is called activity is in pause state. 

1. If onDestroy is called activity is in stop state.

1. Click on the exit button to show alret dialog box.

1. Explain not calling of onPause, as activity is partially visible.

1. Explain the Alert Dialog box in 4 parts


1. Use of AlertDialogBox (ex. Server not responding, New update, Delete Confirmation)


1. Resources are limited and Android Solves it by killing low 
	priority app which you have not used it.
	So Apps can be killed at anytime
	So we need to learn lifecycle of the activity and signals we get from
	framework, so that we can react to it
	
1. Explain the Android Activity Lifecycle

1. Explain the Active Lifetime of an App ( foreground and has focus )
	we start with the OnCreate callback where we build and wire up your UI
	Once that is done, your activity is CREATED
	Then onStart is called to make your activity VISIBLE
	Then onResume is called to make your App ACTIVE and has focus and becomes active foreground app

	The same sequence happens in reverse
	onPause is called to bring your app in PAUSED and focus is lost
	Then onStop is called to make your app STOPPED and your app is not visible 	
	and finally onDestroy indicating the end of the lifecycle by getting DESTROYED

	Active Lifetime and Visible Lifetime happens many times
	So app goes from STOPPED to VISIBLE by calling onRestart


1. Active Lifecycle when in foreground and has focus
	Once it is partially invisible onPause is called and 
	your app goes to the VISIBLE lifetime
	
	
1. Visible Lifetime of an App continues whenever the app is visible 
	

1. What is the sequence of the lifecycle events on rotation

	onPause
	onStop
	onDestroy
	onCreate
	onStart
	onResume
	
1. How should you prepare for an untimely death of your app
	Which is the lifecycle event which would be called guaranteed after honeycomb
	onStop

	Which is the lifecycle event which would be called guaranteed before honeycomb
	onPause

		
1. OnPause and OnStop to clean all resources 
	closing any open connections?


1. Class work : Actvity 1 & 2 with textview and button.

1. Show a ramdom value in textview 1.

1. onclick of button 1 -> open activity 2

1. Show a ramdom value in textview 2. 

1. onClick on button 2 -> back to activity 1. 

1. now change the value of text view 1 using random class.

1. Google 'How to generate random number in android'.
	

1. Optimising the Layout
	Use the Shallow and Wide Approach
	Do not use the Deep and Narrow approach 
	Try to have more siblings and fewer Children
	Flatten your Layout
	Not more than 10 nested Views
	Not more than 80 views in total

1. Give a demo of the Hierarchy Viewer tool
	Select the connected Device
	Select the App
	Show the Tree View and Layout View for the selected app

## Extra Learing

1. How to maintain the state of the Application ?

1. Use of bundles to save the state of the App.

1. Extra Reading

http://developer.android.com/training/basics/activity-lifecycle/recreating.html

1. Extra Learning
http://developer.android.com/guide/components/activities.html

1. Extra Reading 
https://developers.google.com/maps/documentation/android-api/intents


1. Extra Reading
http://developer.android.com/reference/android/content/Intent.html

1. Extra Reading
http://developer.android.com/reference/android/content/Intent.html#ACTION_SEND

	
1. Give a demo of the Static Analysis tool ( Lint Windows )
http://developer.android.com/tools/help/lint.html
Menu->Analyze->Inspect Code

1. Show the URL in Browser for supporting multiple screens
http://developer.android.com/guide/practices/screens_support.html


1. Introduce the concept of MultiScreen Support 

For example, two devices that both report a screen size of normal might have actual screen sizes and aspect ratios that are slightly different when measured by hand.
Similarly, two devices that report a screen density of hdpi might have real pixel densities that are slightly different. 

Note: The characteristics that define a device's generalized screen size and density are independent from each other. 
For example, a WVGA high-density screen is considered a normal size screen because its physical size is about the same as the T-Mobile G1 (Android's first device and baseline screen configuration). 
On the other hand, a WVGA medium-density screen is considered a large size screen. 
Although it offers the same resolution (the same number of pixels), the WVGA medium-density screen has a lower screen density, meaning that each pixel is physically larger and, thus, the entire screen is larger than the baseline (normal size) screen.

T-Mobile G1
Dimensions 	117 x 55.7 x 17.1 mm (4.61 x 2.19 x 0.67 in)
Size 	3.2 inches (~46.8% screen-to-body ratio)
Resolution 	320 x 480 pixels (~180 ppi pixel density)

1. Screen Sizes

    xlarge screens are at least 960dp x 720dp
    large screens are at least 640dp x 480dp
    normal screens are at least 470dp x 320dp
    small screens are at least 426dp x 320dp

THIS IS THE SOLUTION TO THE PROBLEM OF SUPPORTING TO MULTIPLE SCREENS

1. Explicitly declare in the manifest which screen sizes your application supports
	To declare the screen sizes your application supports, you should include the <supports-screens> element in your manifest file.

1. Provide different layouts for different screen sizes
	In most cases, this works fine. 
	In other cases, your UI might not look as good and might need adjustments for different screen sizes. 
	For example, on a larger screen, 
	you might want to adjust the position and size of some elements to take advantage of the additional screen space, 
	or on a smaller screen, you might need to adjust sizes so that everything can fit on the screen.
	layout-sw600dp

1. Provide different bitmap drawables for different screen densities
	drawable-hdpi/
	
	
1. Configuration examples

    320dp: a typical phone screen (240x320 ldpi, 320x480 mdpi, 480x800 hdpi, etc).
    480dp: a tweener tablet like the Streak (480x800 mdpi).
    600dp: a 7� tablet (600x1024 mdpi).
    720dp: a 10� tablet (720x1280 mdpi, 800x1280 mdpi, etc).



1. What are the screen sizes and pixel densities of the Nexus One and Nexus 5 ?
	Students need to google and come to the values and then the bucket

	Nexus One ( 3.7 inch Screen Size = Normal   and Pixel Density = hdpi  252 dpi )
 
	Nexus 5   ( 4.9 inch Screen Size = Normal   and Pixel Density = xxhdpi 445 dpi)

1. Let students calculate the sizes of the icons required. 
   Lets assume that the baseline icon size should be 48x48 pixel


1. Let the students now add the app icons into mipmap folders, 
	introduce and share the tool for converting the icons of different sizes
	
1. Resize tool demo.


1. Create the actual design on photoshop using nexus 5 (1080*1920px).

1. convert that dimens into dp values(divided by 3 as nexus 5 have 3px = 1 dp (3dip) )

1. identify layouts to be used to achieve the login design

1. Identify the views also (image view, 3 edit text, 1 button, 1 text view) 

1. the dotted represent another nested linear-layout.

1. Remove the default padding form root layout.(Margins should always preferred over padding).

1. Drag and drop the layout.

1. Drag and Drop all controls(image view, 3 edit text, 1 button, 1 text view) 

1. Open the text mode and update all the calculated dp values.(height and width)

1. set the dp values for padding and margin.

1. set the gravity.

1. set other properties(textColor,textColorHint,textSize,background,hint,text)

1. Check the layout on nexus 5 screen. it should match the image.

1. Covert the hardcoded dp n sp values into dimens file(default one)

1. Check the layout on nexus 5 screen. there should be not diff.

1. Check the layout file on all screen sizes.

1. identify and make a list of in which the UI is not proper as expected.()

1. Nexus s(4.0",480*800 : hdpi), Nexus 7(7.0",1200*1920 :xhdpi), Nexus 9(8.9",2048*1536 : xhdpi), Nexus 10(10.0",2560*1600:xhdpi)
	320dp: a typical phone screen (240x320 ldpi, 320x480 mdpi, 480x800 hdpi, etc).
	480dp: a tweener tablet like the Streak (480x800 mdpi).
	600dp: a 7� tablet (600x1024 mdpi).
	720dp: a 10� tablet (720x1280 mdpi, 800x1280 mdpi, etc).

1. let fix the first issue for nexus s, itis identify as 320dp so let create a dir values-sw320dp.

1. Copy the dimens file from values folder and update the dp values 10% lesser.

1. Now fix the issue for nexus 7 by increasing the dp values by 20%

1. after adding values dir for 600dp (values-sw600dp) ,the problme is been fixed for 9 and 10.(fallback from default to 600)

1. if you still want to control the dimes in 9 and 10, create 720 dp values folder. 

1. As we have more space in 9 and 10. lets increase the logo size and rest dimens remain same.

1. Now check the nexus 5 layout which was earlier perfect is now distorted.

1. as nexus 5 have width 360dp (1080px/3dpi), so it will pick its dimension from sw320 folder, as it lies in 320-480 range.

1. To correct this problem we make a value folder sw360dp (320-360-480-600-720).

1. Now 5 will its dimension value from 360 folder which have correct dimension for nexus 5.

1. if any device is less then 320, then it will take dimes from values folder

1. if any device is grater then 720 it will take dimension from 720 folder.

1. identify the following parameters
5.2"/1080/1920/420dpi
a.Scale factor = 420/160= 2.6
b.Dp = 1080/2.6 = 415dp
c. Match the Range = sw360 - sw420

7"/1200/1920/xhdpi
a.Scale factor = xhdpi= 2
b.Dp = 1200/2 = 600dp
c. Match the Range = sw600 - sw720